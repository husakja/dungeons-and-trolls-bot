import api from './api.js';

export default {
  status() {
    return api().get("game");
  },

  move(coordinates) {
    return api().post('move', coordinates);
  },

  respawn() {
    return api().post('respawn');
  },

  buy(ids) {
    return api().post('buy', { ids });
  },

  skillPoint(attributes) {
    return api().post('assign-skill-points', attributes);
  },

  yell(message) {
    return api().post('yell', { text: message });
  },

  skill(skillId, targetId) {
    return api().post('skill', { skillId, targetId });
  },

  jump(skillId, position) {
    return api().post('skill', { skillId, position });
  },
};
