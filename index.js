import requests from './requests.js';

let healSkills = ['Doctorly Patch Wounds', 'Carefully Patch Wounds', 'Thrifty Patch Wounds'];
let restSkills = ['Rest', 'Refreshing Rest', 'Energizing Rest'];

function findTarget(data) {
  let targetDoor = null;
  let objs = data.map.levels[0].objects;

  // portal with largest destination
  targetDoor = objs.reduce((i, c) => i.portal && (!c.portal || i?.portal?.destinationFloor > c?.portal?.destinationFloor) ? i : c, objs[0]);
  if (targetDoor && targetDoor.portal) {
    return targetDoor;
  }

  // stairs
  return objs.find(i => i.isStairs);
}

function distance(A, B) {
  let x = A.positionX - B.positionX;
  let y = A.positionY - B.positionY;
  return Math.abs(x) + Math.abs(y);
}

function waitForOthers(data, me) {
  let objects = data.map.levels[0].objects;
  let stairs = objects.find(i => i.isStairs);

  if (distance(me.coordinates, stairs) > 2 || data.currentLevel == 0) {
    return true;
  }

  let players = objects.filter(i => i.players.length).map(i => i.players).flat();

  let maxDist = 0
  let mostDistantPlayer = null
  for (let player of players) {
    if (player.id == data.character.id) {
      continue
    }

    let dist = distance(stairs, player.coordinates)
    if (dist > maxDist && dist > 2) {
      maxDist = dist
      mostDistantPlayer = player
    }
  }

  if (mostDistantPlayer) {
    requests.yell("Spěchej člověče " + mostDistantPlayer.name);
    return false;
  }

  return true;
}

function errorLog(e) {
  console.log(e.response.data.message);
}

function logMyEvents(data) {
  let events = data.events.filter(i => i.message.search("Darik") > -1);
  if (events) {
    for (let event of events) {
      console.log(event.message);
    }
  }
}

function selectItems(data, me) {
  // first level
  let items = [];
  let money = me.money;
  let skillPoints = me.skillPoints;
  let shopItems = data.shopItems.sort((a, b) => a.price - b.price);

  let slots = ["head", "mainHand", "offHand", "body", "legs", "neck"];

  // item with rest
  let restItem = shopItems
    .filter(i => Object.entries(i.requirements).length == 0 ||
      Object.entries(i.requirements).reduce((c, i) => c + i[1], 0) <= skillPoints / 2)
    .filter(i => slots.includes(i.slot)) // only empty slots
    .find(i => i.skills.find(j => restSkills.includes(j.name))); // rest skill
  if (!restItem) {
    console.log("no rest item");
  } else {
    items.push(restItem);
    slots.splice(slots.indexOf(restItem.slot), 1);
    money -= restItem.price;
    skillPoints -= restItem.skillPoints;
  }

  // item with heal
  let healItem = shopItems
    .filter(i => Object.entries(i.requirements).length == 0 ||
      Object.entries(i.requirements).reduce((c, i) => c + i[1], 0) <= skillPoints)
    .filter(i => slots.includes(i.slot)) // only empty slots
    .find(i => i.skills.find(j => healSkills.includes(j.name))); // heal skill
  if (!healItem) {
    console.log("no heal item");
  } else {
    items.push(healItem);
    slots.splice(slots.indexOf(healItem.slot), 1);
    money -= healItem.price;
    skillPoints -= healItem.skillPoints;
  }

  // items with move
  let moveItem = shopItems
    .filter(i => Object.entries(i.requirements).length == 0 ||
      Object.entries(i.requirements).reduce((c, i) => c + i[1], 0) <= skillPoints)
    .filter(i => slots.includes(i.slot)) // only empty slots
    .find(i => i.skills.find(j => j.casterEffects.flags.movement)); // move skill
  if (!moveItem) {
    console.log("no move item");
  } else {
    items.push(moveItem);
    slots.splice(slots.indexOf(moveItem.slot), 1);
    money -= moveItem.price;
    skillPoints -= moveItem.skillPoints;
  }

  // use rest of the money for something with stamina
  for (let slot of slots) {
    let maxStamina = shopItems
      .filter(i => Object.entries(i.requirements).length == 0 ||
        Object.entries(i.requirements).reduce((c, i) => c + i[1], 0) <= skillPoints)
      .filter(i => i.slot == slot)
      .map(i => i.price)
      .reduce((c, i) => i > c ? i : c, 0);
    let item = shopItems.find(i => i.price == maxStamina && i.slot == slot);
    if (item && money > item.price) {
      items.push(item);
      money -= item.price;
      skillPoints -= item.skillPoints;
    }
  }

  return items;
}

async function skillPoints(me, items) {
  let points = { strength: 0, dexterity: 0, intelligence: 0, willpower: 0, constitution: 0, slashResist: 0, pierceResist: 0, fireResist: 0, poisonResist: 0, electricResist: 0, life: 0, stamina: 0, mana: 0 };
  for (let i = 0; i < items.length; i++) {
    for (let attribute in items[i].requirements) {
      points[attribute] += items[i].requirements[attribute];
    }
  }
  let restPoints = me.skillPoints - Object.entries(points).reduce((c, i) => c + i[1], 0);
  points.dexterity += restPoints / 2;
  points.intelligence += restPoints / 2;
  console.log("[POINTS]", points);
  await requests.skillPoint(points).catch(errorLog);
}

function tickStatus(data, me) {
  let tick = data.tick;
  let pos = me.coordinates;
  let x = pos.positionX;
  let y = pos.positionY;
  let equip = me.equip.length;
  let money = me.money;
  let str = `${tick} [${x},${y}] ${equip}e, ${money}\$`;
  console.log(str);
}

function playerToHeal(data) {
  return data.map.levels[0].objects
    .filter(i => i.players.length)
    .map(i => i.players)
    .flat()
    .filter(i => i.attributes.life != i.maxAttributes.life)
    .reduce((c, i) => !c || i.attributes.life < c.attributes.life ? i : c, null);
}

function closestPlayer(data, me) {
  return data.map.levels[0].objects
    .filter(i => i.players.length)
    .map(i => i.players)
    .flat()
    .filter(i => i.name != "Darik")
    .reduce((c, i) => !c || distance(i.coordinates, me.coordinates) < distance(c.coordinates, me.coordinates) ? i : c, null);
}

async function yellColor(message, color) {
  requests.yell(`<b><color=#${color}>${message}</color></b>`).catch(errorLog);
}

async function actions(data, me) {
  // targets
  let targetHeal = playerToHeal(data);
  let targetFollow = closestPlayer(data, me);
  let targetDoor = findTarget(data);
  let isWaitForOthers = waitForOthers(data, me);

  // skill ids
  let healSkill = me.equip.map(i => i.skills).flat().find(i => healSkills.includes(i.name));
  let restSkill = me.equip.map(i => i.skills).flat().find(i => restSkills.includes(i.name));
  let moveSkill = me.equip.map(i => i.skills).flat().find(i => i.casterEffects.flags.movement);

  if (me.attributes.stamina < me.maxAttributes.stamina) {
    await requests.skill(restSkill.id).catch(errorLog);
    yellColor("Resting", "FFF000");
  }

  if (data.currentLevel > 0 && targetHeal) {
    let d = distance(me.coordinates, targetHeal.coordinates);

    if (healSkill && d < healSkill.range.constant) {
      await requests.skill(healSkill.id, targetHeal.id).catch(errorLog);
      yellColor("Healing " + targetHeal.name, "00FF00");
    } else {
      await requests.move(targetHeal.coordinates).catch(errorLog);
      yellColor("Will heal " + targetHeal.name, "FF6600");
    }
  } else if (data.currentLevel > 0 && targetFollow) {
    await requests.move(targetFollow.coordinates).catch(errorLog);
    yellColor("Follow " + targetFollow.name, "00FFFF");
  }
  else if (targetDoor && isWaitForOthers) { // go towards stairs or portal
    await requests.move(targetDoor.position).catch(errorLog);
    yellColor("Doors", "FFFFFF");
  }
}

async function tick() {
  await requests.status()
    .then(async r => {
      let me = r.data.character;

      // log
      tickStatus(r.data, me);
      // logMyEvents(r.data);

      // init
      if (me.equip.length == 0) {
        let items = selectItems(r.data, r.data.character);
        await skillPoints(r.data.character, items);
        await requests.buy(items.map(i => i.id)).catch(errorLog);
        yellColor("Ready!", '00FFFF');
        return;
      }

      await actions(r.data, me);
    })
}

await requests.respawn();
while (true) {
  await tick();
}
