import axios from 'axios';

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.withCredentials = true;

export default () => {
  const api = axios.create({
    baseURL: "http://10.0.1.63/v1/",
    headers: {
      Accept: 'application/json',
      "x-api-key": "adb3fd9d-d093-45b4-bfc0-0e375b3c03d2",
    }
  });

  return api;
}
